﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatePlatform : MonoBehaviour {

	public Texture2D platformTexture;

	// Use this for initialization
	void Start () {
		this.GetComponent<Renderer>().material.mainTexture = platformTexture;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
