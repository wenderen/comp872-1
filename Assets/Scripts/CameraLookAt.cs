﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLookAt : MonoBehaviour {

	public bool isFalling;
	public GameObject camera;

	Vector3 lookAtPos = new Vector3 (0, 5, 0);
	float v = 0;
	const float g = 4.9f;

	int countdown = 0;

	float h = 1/60.0f; // timestep

	void OnTriggerExit(Collider other) {
		isFalling = true;
		Debug.Log ("left the platform");
	}

	// Use this for initialization
	void Start () {
		isFalling = false;
		Vector3 lookAtPos = new Vector3 (0, 51.5f, 0);
		camera.transform.LookAt (lookAtPos);
	}
	
	// Update is called once per frame
	void Update () {
		if (!isFalling) {
			return;
		}

		float y = camera.transform.position.y;
		y += h * v;
		camera.transform.position = new Vector3 (camera.transform.position.x, y, camera.transform.position.z);
		v -= h * g;

		if (y < 5.0f) {
			if (countdown == 0) {
				v = -0.75f * v;
				countdown = 10;
			} else {
				countdown--;
			}
		}
	}
}
